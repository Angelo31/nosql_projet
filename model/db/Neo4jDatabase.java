package com.camillepradel.movierecommender.model.db;

import com.camillepradel.movierecommender.model.Genre;
import com.camillepradel.movierecommender.model.Movie;
import com.camillepradel.movierecommender.model.Rating;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.neo4j.driver.*;

import static org.neo4j.driver.Values.parameters;

public class Neo4jDatabase extends AbstractDatabase implements AutoCloseable {

	private final Driver driver;

	public Neo4jDatabase() {
		driver = GraphDatabase.driver("bolt://127.0.0.1:7687", AuthTokens.basic("neo4j", "123456"));
	}

	public void close() throws Exception {
		driver.close();
	}

	@Override
	public List<Movie> getAllMovies() {

		List<Movie> movies = new LinkedList<Movie>();
		HashMap<Integer, Genre> allGenres = getAllGenres();

		try (Session session = driver.session()) {
			org.neo4j.driver.Result result;
			org.neo4j.driver.Transaction transac = session.beginTransaction();

			result = transac.run(
					"MATCH p=(m:Movie)-[r:CATEGORIZED_AS]->(g:Genre) RETURN m.id as movie_id, m.title as movie_title, collect(g.id) as genreids order by movie_id");

			while (result.hasNext()) {
				Record rec = result.next();
				int movieId = rec.get("movie_id").asInt();
				String movieTitle = rec.get("movie_title").asString();
				List<Integer> genreIds = rec.get("genreids").asList(Values.ofInteger());

				List<Genre> movieGenres = new LinkedList<>();
				for (int genreId : genreIds) {
					movieGenres.add(allGenres.get(genreId));	
				}
				movies.add(new Movie(movieId, movieTitle, movieGenres));
			}

		}

		return movies;
	}

	HashMap<Integer, Genre> getAllGenres() {
		HashMap<Integer, Genre> allGenres = new HashMap<>();

		try (Session session = driver.session()) {
			org.neo4j.driver.Result result;
			org.neo4j.driver.Transaction transac = session.beginTransaction();
			result = transac.run("match(g:Genre) return g");

			while (result.hasNext()) {

				Record rec = result.next();

				for (Value s : rec.values()) {
					int genreid = s.get("id").asInt();
					String genrename = s.get("name").asString();
					allGenres.put(genreid, new Genre(genreid, genrename));
				}
			}
		}
		return allGenres;
	}

	@Override
	public List<Movie> getMoviesRatedByUser(int userId) {
		List<Movie> movies = new LinkedList<Movie>();
		HashMap<Integer, Genre> allGenres = getAllGenres();

		try (Session session = driver.session()) {
			org.neo4j.driver.Result result;
			org.neo4j.driver.Transaction transac = session.beginTransaction();

			Map<String,Object> params = new HashMap<>();
			params.put("id",userId);
			
			result = transac.run(
					"MATCH p=(u:User)-[r:RATED]->(m:Movie)-[c:CATEGORIZED_AS]->(g:Genre) WHERE u.id = $id  RETURN m.title as movie_title, m.id as movie_id, r.note, collect(g.id) as genreids ORDER BY m.id", params);

			while (result.hasNext()) {
				Record rec = result.next();

				int movieId = rec.get("movie_id").asInt();
				String movieTitle = rec.get("movie_title").asString();
				List<Integer> genreIds = rec.get("genreids").asList(Values.ofInteger());

				List<Genre> movieGenres = new LinkedList<>();
				
				for (int genreId : genreIds) {
					movieGenres.add(allGenres.get(genreId));
				}
				movies.add(new Movie(movieId, movieTitle, movieGenres));

			}

		}
		return movies;
	}

	@Override
	public List<Rating> getRatingsFromUser(int userId) {
		List<Rating> ratings = new LinkedList<Rating>();
		HashMap<Integer, Genre> allGenres = getAllGenres();

		try (Session session = driver.session()) {
			org.neo4j.driver.Result result;
			org.neo4j.driver.Transaction transac = session.beginTransaction();

			Map<String,Object> params = new HashMap<>();
			params.put("id",userId);
			
			result = transac.run(
					"MATCH p=(u:User)-[r:RATED]->(m:Movie)-[c:CATEGORIZED_AS]->(g:Genre) WHERE u.id = $id  RETURN m.title as movie_title, m.id as movie_id, r.note as rating, collect(g.id) as genreids ORDER BY m.id", params);

			while (result.hasNext()) {
				Record rec = result.next();

				int movieId = rec.get("movie_id").asInt();
				int rating = rec.get("rating").asInt();
				String movieTitle = rec.get("movie_title").asString();
				List<Integer> genreIds = rec.get("genreids").asList(Values.ofInteger());

				List<Genre> movieGenres = new LinkedList<>();
				
				for (int genreId : genreIds) {
					movieGenres.add(allGenres.get(genreId));
				}
				ratings.add(new Rating(new Movie(movieId, movieTitle, movieGenres),userId,rating));

			}

		}
		return ratings;
	}

	@Override
	public void addOrUpdateRating(Rating rating) {
		// TODO: add query which
		// - add rating between specified user and movie if it doesn't exist
		// - update it if it does exist
		

		try (Session session = driver.session()) {

			//org.neo4j.driver.Transaction transac = session.beginTransaction();

			Map<String,Object> params = new HashMap<>();

			params.put("userid",rating.getUserId());
			params.put("movieid",rating.getMovieId());
			params.put("score",rating.getScore());
			try {
				String greeting = session.writeTransaction( tx ->
	            {
	                Result result = tx.run( "MATCH p=(u:User{id:$userid})-[r:RATED]->(m:Movie{id:$movieid}) SET r.note = $score RETURN m.title as movie_title, m.id as movie_id, r.note as rating ORDER BY m.id",
	                                        params );
	                return result.single().get( 0 ).asString();
	            } );
			} catch(Exception e) {
				
				String greeting = session.writeTransaction( tx ->
	            {
	                Result result = tx.run( "MATCH (u:User),(m:Movie) WHERE u.id = $userid  AND m.id = $movieid CREATE (u)-[r:RATED]->(m) SET r.note = $score RETURN m.title as movie_title, m.id as movie_id, r.note as rating ORDER BY m.id",
	                                        params );
	                return result.single().get( 0 ).asString();
	            } );
	            
			}
		}
		
	}

	@Override
	public List<Rating> processRecommendationsForUser(int userId, int processingMode) {
		// TODO: process recommendations for specified user exploiting other users
		// ratings
		// use different methods depending on processingMode parameter
		Genre genre0 = new Genre(0, "genre0");
		Genre genre1 = new Genre(1, "genre1");
		Genre genre2 = new Genre(2, "genre2");
		List<Rating> recommendations = new LinkedList<Rating>();
		String titlePrefix;
		if (processingMode == 0) {
			titlePrefix = "0_";
		} else if (processingMode == 1) {
			titlePrefix = "1_";
		} else if (processingMode == 2) {
			titlePrefix = "2_";
		} else {
			titlePrefix = "default_";
		}
		recommendations.add(new Rating(
				new Movie(0, titlePrefix + "Titre 0", Arrays.asList(new Genre[] { genre0, genre1 })), userId, 5));
		recommendations.add(new Rating(
				new Movie(1, titlePrefix + "Titre 1", Arrays.asList(new Genre[] { genre0, genre2 })), userId, 5));
		recommendations.add(
				new Rating(new Movie(2, titlePrefix + "Titre 2", Arrays.asList(new Genre[] { genre1 })), userId, 4));
		recommendations.add(
				new Rating(new Movie(3, titlePrefix + "Titre 3", Arrays.asList(new Genre[] { genre0, genre1, genre2 })),
						userId, 3));
		return recommendations;
	}
}

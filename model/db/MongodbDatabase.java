package com.camillepradel.movierecommender.model.db;

import com.camillepradel.movierecommender.model.Genre;
import com.camillepradel.movierecommender.model.Movie;
import com.camillepradel.movierecommender.model.Rating;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.neo4j.driver.Record;
import org.neo4j.driver.Session;
import org.neo4j.driver.Values;

public class MongodbDatabase extends AbstractDatabase {

	
	private MongoDatabase database;
	
	public MongodbDatabase() {
		MongoClient mongoClient = new MongoClient("localhost" , 27017);
		
		database = mongoClient.getDatabase("MyDataBase");
		
		
	}
	
	
    @Override
    public List<Movie> getAllMovies() {
        List<Movie> movies = new LinkedList<Movie>();
    	MongoCollection<Document> collection = database.getCollection("movies");
		MongoCursor<Document> cursor = collection.find().iterator();
		
		try {
		    while (cursor.hasNext()) {
		        Document doc = cursor.next();
		        int movieId = (int) doc.get("_id");
		        String movieTitle = (String) doc.get("title");
                String genreNames = doc.get("genres").toString();

                List<Genre> movieGenres = new LinkedList<>();
                String genre = "";
                for (int i = 0; i < genreNames.split("|").length; i++) {
                    String genreId = genreNames.split("|")[i];
                    if (genreId.equals("|")) {
                        movieGenres.add(new Genre(0, genre));
                        genre = "";
                    } 
                    else if(i == genreNames.split("|").length - 1) {
                        genre += genreId;
                        movieGenres.add(new Genre(0, genre));
                        genre = "";
                    }
                	else {
                        genre += genreId;
                    }
                }
                movies.add(new Movie(movieId, movieTitle, movieGenres));
		    }
		} finally {
		    cursor.close();
		}
    	
        return movies;
    }

    @Override
    public List<Movie> getMoviesRatedByUser(int userId) {
        List<Movie> movies = new LinkedList<Movie>();
        MongoCollection<Document> collectionUsers = database.getCollection("users");
        MongoCollection<Document> collectionMovies = database.getCollection("movies");
        
        BasicDBObject movieByUser = new BasicDBObject();
        movieByUser.put("_id", userId);
        MongoCursor<Document> cursorUser = collectionUsers.find(movieByUser).iterator();
        
        if(cursorUser.hasNext()) {
        	Document doc = cursorUser.next();
        	ArrayList<Object> truc = (ArrayList<Object>) doc.get("movies");
        	for(Object obj : truc) {
        		// D�sol�..
        		String strAfter = obj.toString().split("movieid=")[1];
        		String strBefore = strAfter.split(",")[0];
        		int movieId = Integer.valueOf(strBefore);
        		
                BasicDBObject movieById = new BasicDBObject();
                movieById.put("_id", movieId);
                
                MongoCursor<Document> cursorMovies = collectionMovies.find(movieById).iterator();
        		
                if(cursorMovies.hasNext()) {
                	Document elt2 = cursorMovies.next();           	
                	String movieTitle = elt2.get("title").toString();
                	String genreNames = elt2.get("genres").toString();
                    List<Genre> movieGenres = new LinkedList<>();
                    
                    String genre = "";
                    for (int i = 0; i < genreNames.split("|").length; i++) {
                        String genreId = genreNames.split("|")[i];
                        if (genreId.equals("|")) {
                            movieGenres.add(new Genre(0, genre));
                            genre = "";
                        } 
                        else if(i == genreNames.split("|").length - 1) {
                            genre += genreId;
                            movieGenres.add(new Genre(0, genre));
                            genre = "";
                        }
                    	else {
                            genre += genreId;
                        }
                    }
    				movies.add(new Movie(movieId, movieTitle, movieGenres));
                }
        	}
        }
        
        return movies;
    }

    @Override
    public List<Rating> getRatingsFromUser(int userId) {
    	List<Rating> ratings = new LinkedList<Rating>();

    	MongoCollection<Document> collectionUsers = database.getCollection("users");
        MongoCollection<Document> collectionMovies = database.getCollection("movies");
        
        BasicDBObject movieByUser = new BasicDBObject();
        movieByUser.put("_id", userId);
        MongoCursor<Document> cursorUser = collectionUsers.find(movieByUser).iterator();
        
        if(cursorUser.hasNext()) {
        	Document doc = cursorUser.next();
        	ArrayList<Object> truc = (ArrayList<Object>) doc.get("movies");
        	for(Object obj : truc) {
        		// Sorry..
        		String strAfterMovieId = obj.toString().split("movieid=")[1];
        		String strBefore = strAfterMovieId.split(",")[0];
        		int movieId = Integer.valueOf(strBefore);
        		
        		// Sorry x 2 ..
        		String strAfterRating = obj.toString().split("rating=")[1];
        		String strBeforeComa = strAfterRating.split(",")[0];
        		int rating = Integer.valueOf(strBeforeComa);
        		
                BasicDBObject movieById = new BasicDBObject();
                movieById.put("_id", movieId);
                MongoCursor<Document> cursorMovies = collectionMovies.find(movieById).iterator();
        		
                if(cursorMovies.hasNext()) {
                	Document doc2 = cursorMovies.next();           	
                	String movieTitle = doc2.get("title").toString();
                	String genreNames = doc2.get("genres").toString();
                    List<Genre> movieGenres = new LinkedList<>();
                    
                    String genre = "";
                    for (int i = 0; i < genreNames.split("|").length; i++) {
                        String genreId = genreNames.split("|")[i];
                        if (genreId.equals("|")) {
                            movieGenres.add(new Genre(0, genre));
                            genre = "";
                        } else if(i == genreNames.split("|").length - 1) {
                            genre += genreId;
                            movieGenres.add(new Genre(0, genre));
                            genre = "";
                        }
                    	else {
                            genre += genreId;
                        }
                    }
            		ratings.add(new Rating(new Movie(movieId, movieTitle, movieGenres),userId,rating));
                }
        	}
        }
        
		return ratings;
    }

    @Override
    public void addOrUpdateRating(Rating rating) {

        MongoCollection<Document> collectionUsers = database.getCollection("users");
        BasicDBObject movieByUser = new BasicDBObject();
        movieByUser.put("_id", rating.getUserId());
        MongoCursor<Document> cursorUser = collectionUsers.find(movieByUser).iterator();
        if(cursorUser.hasNext()) {
        	Document elt = cursorUser.next();
        	ArrayList<Object> truc = (ArrayList<Object>) elt.get("movies");
        	for(Object obj : truc) {
        	
        	}
        }
        // To be continued...
    }

    @Override
    public List<Rating> processRecommendationsForUser(int userId, int processingMode) {
        // TODO: process recommendations for specified user exploiting other users ratings
        //       use different methods depending on processingMode parameter
        Genre genre0 = new Genre(0, "genre0");
        Genre genre1 = new Genre(1, "genre1");
        Genre genre2 = new Genre(2, "genre2");
        List<Rating> recommendations = new LinkedList<Rating>();
        String titlePrefix;
        if (processingMode == 0) {
            titlePrefix = "0_";
        } else if (processingMode == 1) {
            titlePrefix = "1_";
        } else if (processingMode == 2) {
            titlePrefix = "2_";
        } else {
            titlePrefix = "default_";
        }
        recommendations.add(new Rating(new Movie(0, titlePrefix + "Titre 0", Arrays.asList(new Genre[]{genre0, genre1})), userId, 5));
        recommendations.add(new Rating(new Movie(1, titlePrefix + "Titre 1", Arrays.asList(new Genre[]{genre0, genre2})), userId, 5));
        recommendations.add(new Rating(new Movie(2, titlePrefix + "Titre 2", Arrays.asList(new Genre[]{genre1})), userId, 4));
        recommendations.add(new Rating(new Movie(3, titlePrefix + "Titre 3", Arrays.asList(new Genre[]{genre0, genre1, genre2})), userId, 3));
        return recommendations;
    }    
}
